package com.fx.oa.leave.model;

import com.fx.oa.login.OaUser;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 请假申请
 */
public class FillLeave {

    /**
     * 请假人信息
     */
    private OaUser oaUser;


    /**
     * 请假类型
     */
    private String leaveType;

    /**
     * 请假原因
     */
    private String leaveCause;

    /**
     * 提交时间
     */
    private String fillTime;

    /**
     * 请假日期- 启
     */
    private String startDate;

    /**
     * 请假时间- 启
     */
    private String startTime;

    /**
     * 请假日期- 止
     */
    private String endDate;

    /**
     * 请假时间- 止
     */
    private String endTime;

    /**
     * 请假小时数
     */
    private BigDecimal leaveTime;


    public OaUser getOaUser() {
        return oaUser;
    }

    public void setOaUser(OaUser oaUser) {
        this.oaUser = oaUser;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getLeaveCause() {
        return leaveCause;
    }

    public void setLeaveCause(String leaveCause) {
        this.leaveCause = leaveCause;
    }

    public String getFillTime() {
        return fillTime;
    }

    public void setFillTime(String fillTime) {
        this.fillTime = fillTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(BigDecimal leaveTime) {
        this.leaveTime = leaveTime;
    }
}
