package com.fx.oa.leave;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fx.oa.http.HttpClient;
import com.fx.oa.leave.model.FillLeave;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    public TextArea cause;
    public DatePicker startDate;
    public DatePicker endDate;
    public TextField allTime;
    public ChoiceBox<LeaveType> leaveType;
    public ChoiceBox<String> start_h;
    public ChoiceBox<String> start_m;
    public ChoiceBox<String> end_h;
    public ChoiceBox<String> end_m;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createLeaveType();
        start_h.getItems().setAll(Util.getHour());
        start_m.getItems().setAll(Util.getMinute());
        end_h.getItems().setAll(Util.getHour());
        end_m.getItems().setAll(Util.getMinute());

    }

    /**
     * 初始化请假选择框
     */
    private void createLeaveType() {
        List<LeaveType> list = new ArrayList<>(5);
        LeaveType leaveType1 = new LeaveType();
        leaveType1.setLeaveName("事假");
        list.add(leaveType1);
        LeaveType leaveType2 = new LeaveType();
        leaveType2.setLeaveName("病假");
        list.add(leaveType2);
        LeaveType leaveType3 = new LeaveType();
        leaveType3.setLeaveName("产假");
        list.add(leaveType3);
        LeaveType leaveType4 = new LeaveType();
        leaveType4.setLeaveName("丧假");
        list.add(leaveType4);
        leaveType.getItems().addAll(list);


    }

    /**
     * 取消按钮单击事件
     *
     * @param actionEvent
     */
    public void cancelAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/fxml/clocking.fxml")));
        cause.getScene().setRoot(root);
    }

    /**
     * 提交按钮单击事件
     *
     * @param actionEvent
     */
    public void submitAction(ActionEvent actionEvent) throws JsonProcessingException {
        FillLeave fillLeave = new FillLeave();
        fillLeave.setOaUser(Constant.oaUser);
        fillLeave.setLeaveType(leaveType.getValue().leaveName);
        fillLeave.setLeaveCause(cause.getText());
        fillLeave.setFillTime(LocalDateTime.now().toString());
        fillLeave.setStartDate(startDate.getValue().toString());
        LocalTime startTime = LocalTime.of(Integer.parseInt(start_h.getValue()), Integer.parseInt(start_m.getValue()));
        fillLeave.setStartTime(startTime.toString());
        fillLeave.setEndDate(endDate.getValue().toString());
        LocalTime endTime = LocalTime.of(Integer.parseInt(end_h.getValue()), Integer.parseInt(end_m.getValue()));
        fillLeave.setEndTime(endTime.toString());
        fillLeave.setLeaveTime(new BigDecimal(allTime.getText()));

        Result result = HttpClient.doPost("/fill/leave/v1", fillLeave, new TypeReference<Result>() {
        });
        if (result.getSuccess()) {
            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "请假申请", "请假申请提交成功");
        }
    }

    static class LeaveType {

        private Integer key;

        private String leaveName;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getLeaveName() {
            return leaveName;
        }

        public void setLeaveName(String leaveName) {
            this.leaveName = leaveName;
        }

        @Override
        public String toString() {
            return leaveName;
        }
    }


}
