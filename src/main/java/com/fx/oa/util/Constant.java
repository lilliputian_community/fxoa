package com.fx.oa.util;

import com.fx.oa.home.model.OaDep;
import com.fx.oa.login.OaUser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.List;

public class Constant {

    public static final String HTTP_IP = "http://192.168.0.104:8080";

    public static String IP = "";

    public static String MAC = "";

    private static Stage stage;

    public static Stage getStage() {
        if (stage == null) {
            stage = new Stage(StageStyle.UTILITY);
        }
        stage.close();
        return stage;
    }

    /**
     * 内存数据
     */
    public static OaDep oaDep;


    public static List<OaDep> depList;

    /**
     * 登录用户
     */
    public static OaUser oaUser;

}
