package com.fx.oa.util;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Util {

    /**
     * 弹出一个通用的确定对话框
     *
     * @param p_header  对话框的信息标题
     * @param p_message 对话框的信息
     * @return 用户点击了是或否
     */
    public static boolean f_alert_confirmDialog(Alert.AlertType alertType, String p_header, String p_message) {
//        按钮部分可以使用预设的也可以像这样自己 new 一个
        Alert _alert = new Alert(alertType, p_message, new ButtonType("取消", ButtonBar.ButtonData.NO),
                new ButtonType("确认", ButtonBar.ButtonData.YES));
//        设置窗口的标题
        _alert.setTitle("提示");
        _alert.setHeaderText(p_header);
//        设置对话框的 icon 图标，参数是主窗口的 stage
//        showAndWait() 将在对话框消失以前不会执行之后的代码
        _alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> _buttonType = _alert.showAndWait();
//        根据点击结果返回
        if (_buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获得小时列表
     *
     * @return
     */
    public static List<String> getHour() {
        List<String> list = new ArrayList<>(24);
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }

    /**
     * 获取分钟列表
     * @return
     */
    public static List<String> getMinute() {
        List<String> list = new ArrayList<>(60);
        for (int i = 0; i < 60; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }
}
