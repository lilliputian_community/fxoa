package com.fx.oa;


import com.fx.oa.util.Constant;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.lang.reflect.Parameter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class OaLocation extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        primaryStage.setTitle("FX考勤管理系统");
        primaryStage.setScene(new Scene(root, 800, 550));
        primaryStage.getIcons().add(new Image(getClass().getResource("/static/logo.png").toString()));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) throws UnknownHostException, SocketException {
        InetAddress addr = InetAddress.getLocalHost();
        Constant.IP = addr.getHostAddress();
        System.out.println("Local HostAddress:" + addr.getHostAddress());
        String hostname = addr.getHostName();
        System.out.println("Local host name: " + hostname);
        getLocalMac(addr);
        System.setProperty("token","254235432534");
        System.out.println(System.getProperty("token"));
        launch(args);

    }

    private static void getLocalMac(InetAddress ia) throws SocketException {
        //获取网卡，获取地址
        byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
        //System.out.println("mac数组长度："+mac.length);
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < mac.length; i++) {
            if (i != 0) {
                sb.append("-");
            }
            //字节转换为整数
            int temp = mac[i] & 0xff;
            String str = Integer.toHexString(temp);
            //System.out.println("每8位:"+str);
            if (str.length() == 1) {
                sb.append("0" + str);
            } else {
                sb.append(str);
            }
        }
        Constant.MAC = sb.toString().toUpperCase();
        System.out.println("本机MAC地址:" + sb.toString().toUpperCase());
    }
}
