package com.fx.oa.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fx.oa.home.model.AddDep;
import com.fx.oa.http.HttpClient;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import org.springframework.util.StringUtils;

import java.net.URL;
import java.util.ResourceBundle;

public class EditDep implements Initializable {

    /**
     * 编辑
     */
    public TextField edit_depName;
    public TextField edit_depLeader;

    public void edit_emptyAction(ActionEvent actionEvent) {
        edit_depName.setText("");
        edit_depLeader.setText("");
    }

    public void edit_submitAction(ActionEvent actionEvent) throws JsonProcessingException {
        if (!StringUtils.hasLength(edit_depName.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "部门名称", "请输入部门名称");
            return;
        }
        if (!StringUtils.hasLength(edit_depLeader.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "部门主管", "请输入部门主管");
            return;
        }
        Constant.oaDep.setName(edit_depName.getText());
        Constant.oaDep.setLeader(edit_depLeader.getText());

        Result result = HttpClient.doPost("/dep/edit/v1", Constant.oaDep, new TypeReference<Result>() {
        });
        if (result.getSuccess()) {
            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "成功", "部门修改成功");
        } else {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "失败", result.getMsg());
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        edit_depName.setText(Constant.oaDep.getName());
        edit_depLeader.setText(Constant.oaDep.getLeader());
    }
}
