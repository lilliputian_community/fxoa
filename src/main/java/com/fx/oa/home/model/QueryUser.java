package com.fx.oa.home.model;

public class QueryUser {

    private String name;

    private String dep;

    public QueryUser() {
    }

    public QueryUser(String name, String dep) {
        this.name = name;
        this.dep = dep;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }
}
