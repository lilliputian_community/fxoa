package com.fx.oa.home.model;

public class AddDep {

    private String depName;

    private String depLeader;

    public AddDep() {
    }

    public AddDep(String depName, String depLeader) {
        this.depName = depName;
        this.depLeader = depLeader;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getDepLeader() {
        return depLeader;
    }

    public void setDepLeader(String depLeader) {
        this.depLeader = depLeader;
    }
}
