package com.fx.oa.home.model;

public class AddUser {

    private String name;

    private String dep;

    private String post;

    public AddUser() {
    }

    public AddUser(String name, String dep, String post) {
        this.name = name;
        this.dep = dep;
        this.post = post;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
}
