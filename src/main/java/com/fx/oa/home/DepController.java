package com.fx.oa.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fx.oa.home.model.AddDep;
import com.fx.oa.home.model.OaDep;
import com.fx.oa.http.HttpClient;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import org.springframework.util.StringUtils;

import java.util.List;


public class DepController {
    /**
     * 新增
     */
    public TextField depName;
    public TextField depLeader;


    /**
     * 清空表单
     *
     * @param actionEvent
     */
    public void emptyAction(ActionEvent actionEvent) {
        depName.setText("");
        depLeader.setText("");
    }

    /**
     * 提交数据
     *
     * @param actionEvent
     */
    public void submitAction(ActionEvent actionEvent) throws JsonProcessingException {
        if (!StringUtils.hasLength(depName.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "部门名称", "请输入部门名称");
            return;
        }
        if (!StringUtils.hasLength(depLeader.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "部门主管", "请输入部门主管");
            return;
        }
        AddDep addDep = new AddDep(depName.getText(), depLeader.getText());
        Result result = HttpClient.doPost("/dep/v1", addDep, new TypeReference<Result>() {
        });
        if (result.getSuccess()) {
            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "成功", "部门添加成功");
            Result<List<OaDep>> depList = HttpClient.doGet("/dep/list/v1", null, new TypeReference<Result<List<OaDep>>>() {
            });
            Constant.depList = depList.getData();
        } else {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "失败", result.getMsg());
        }

    }


}
