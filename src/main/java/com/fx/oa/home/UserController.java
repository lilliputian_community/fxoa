package com.fx.oa.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fx.oa.home.model.AddUser;
import com.fx.oa.home.model.OaDep;
import com.fx.oa.http.HttpClient;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.springframework.util.StringUtils;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class UserController implements Initializable {
    public TextField u_name;
    public ChoiceBox<OaDep> u_dep;
    public TextField u_post;

    /**
     * 清空表单
     *
     * @param actionEvent
     */
    public void emptyAction(ActionEvent actionEvent) {
        u_name.setText("");
        u_post.setText("");
        u_dep.setValue(null);
    }

    /**
     * 提交数据
     *
     * @param actionEvent
     */
    public void submitAction(ActionEvent actionEvent) throws JsonProcessingException {
        if (!StringUtils.hasLength(u_name.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "代号", "请输入员工代号");
            return;
        }
        if (!StringUtils.hasLength(u_post.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "员工职位", "请输入员工职位");
            return;
        }
        if (Objects.isNull(u_dep.getValue())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "所属部门", "请选择所属部门");
            return;
        }
        AddUser addUser = new AddUser(u_name.getText(), u_dep.getValue().getName(), u_post.getText());
        Result result = HttpClient.doPost("/user/v1", addUser, new TypeReference<Result>() {
        });
        if (result.getSuccess()) {
            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "新增员工", "新增员工操作成功");
            emptyAction(null);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        u_dep.getItems().setAll(Constant.depList);
    }
}
