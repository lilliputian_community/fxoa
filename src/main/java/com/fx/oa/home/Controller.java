package com.fx.oa.home;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fx.oa.home.model.OaDep;
import com.fx.oa.home.model.OaUser;
import com.fx.oa.home.model.QueryUser;
import com.fx.oa.http.HttpClient;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ObservableValueBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private List<OaUser> list;

    QueryUser queryUser = new QueryUser();


    /**
     * 部门
     */
    public TableView<OaDep> depTab;
    public TableColumn<OaDep, Long> depId;
    public TableColumn<OaDep, String> depName;
    public TableColumn<OaDep, String> depLeader;
    public TableColumn<OaDep, HBox> depHandle;

    public TextField qu_name;
    public ChoiceBox<OaDep> que_dep;

    /**
     * 员工
     */
    public TableView<OaUser> userTbl;
    public TableColumn<OaUser, Long> userId;
    public TableColumn<OaUser, String> userName;
    public TableColumn<OaUser, String> userDep;
    public TableColumn<OaUser, String> userPost;
    public TableColumn<OaUser, String> userIp;
    public TableColumn<OaUser, HBox> userHandle;


    public void addUser(ActionEvent actionEvent) throws IOException {
        Stage stage = Constant.getStage();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/sys/add_user.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.getIcons().add(new Image(getClass().getResource("/static/logo.png").toString()));
        stage.setResizable(false);
        stage.setTitle("新增员工");
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initDep();
        try {
            initUser();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化员工数据
     */
    private void initUser() throws JsonProcessingException {
        que_dep.getItems().addAll(Constant.depList);
        userId.setCellValueFactory(new PropertyValueFactory<>("id"));
        userName.setCellValueFactory(new PropertyValueFactory<>("name"));
        userDep.setCellValueFactory(new PropertyValueFactory<>("dep"));
        userPost.setCellValueFactory(new PropertyValueFactory<>("post"));
        userIp.setCellValueFactory(new PropertyValueFactory<>("loginIp"));
        userHandle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OaUser, HBox>, ObservableValue<HBox>>() {
            @Override
            public ObservableValue<HBox> call(TableColumn.CellDataFeatures<OaUser, HBox> param) {
                ObservableValue<HBox> observableValue = new ObservableValueBase<HBox>() {
                    OaUser oaUser = param.getValue();
                    @Override
                    public HBox getValue() {
                        HBox hBox = new HBox();
                        Button del = new Button("删除");
                        del.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                boolean delQrl = Util.f_alert_confirmDialog(Alert.AlertType.WARNING, "删除", "是否要删除<" + oaUser.getName() + ">员工");
                                if (delQrl) {
                                    try {
                                        Result result = HttpClient.doPost("/user/del/v1", oaUser, new TypeReference<Result>() {
                                        });
                                        if (result.getSuccess()) {
                                            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "操作", "删除成功");
                                            queAction(null);
                                        }
                                    } catch (JsonProcessingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        Button cleanIp = new Button("清除IP");
                        cleanIp.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                boolean delQrl = Util.f_alert_confirmDialog(Alert.AlertType.WARNING, "清除IP", "是否要清除<" + oaUser.getName() + ">的登录IP");
                                if(delQrl){
                                    try {
                                        Result result = HttpClient.doPost("/user/clean/ip/v1", oaUser, new TypeReference<Result>() {
                                        });
                                        if (result.getSuccess()) {
                                            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "操作", "清除成功");
                                            queAction(null);
                                        }
                                    } catch (JsonProcessingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        Insets insets = new Insets(0, 0, 0, 15);
                        hBox.setPadding(insets);
                        hBox.setSpacing(20);
                        if(!oaUser.getAdmin()){
                            hBox.getChildren().setAll(del,cleanIp);
                        }
                        return hBox;
                    }
                };
                return observableValue;
            }
        });
        Result<List<OaUser>> result = HttpClient.doPost("/user/list/v1", queryUser, new TypeReference<Result<List<OaUser>>>() {
        });
        list = result.getData();
        userTbl.getItems().setAll(list);
    }

    /**
     * 初始化部门数据
     */
    private void initDep() {
        depId.setCellValueFactory(new PropertyValueFactory<>("id"));
        depName.setCellValueFactory(new PropertyValueFactory<>("name"));
        depLeader.setCellValueFactory(new PropertyValueFactory<>("leader"));
        Result<List<OaDep>> depList = HttpClient.doGet("/dep/list/v1", null, new TypeReference<Result<List<OaDep>>>() {
        });
        Constant.depList = depList.getData();
        depHandle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OaDep, HBox>, ObservableValue<HBox>>() {
            @Override
            public ObservableValue<HBox> call(TableColumn.CellDataFeatures<OaDep, HBox> param) {
                ObservableValue<HBox> observableValue = new ObservableValueBase<HBox>() {
                    OaDep oaDep = param.getValue();

                    @Override
                    public HBox getValue() {
                        HBox hBox = new HBox();
                        Button del = new Button("删除");
                        del.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                boolean delQrl = Util.f_alert_confirmDialog(Alert.AlertType.WARNING, "删除", "是否要删除" + oaDep.getName() + "部门");
                                if (delQrl) {
                                    try {
                                        Result result = HttpClient.doPost("/dep/del/v1", oaDep, new TypeReference<Result>() {
                                        });
                                        if (result.getSuccess()) {
                                            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "操作", "删除成功");
                                            depRelod(null);
                                        }
                                    } catch (JsonProcessingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });

                        Button edit = new Button("编辑");
                        edit.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                Constant.oaDep = oaDep;
                                Stage stage = Constant.getStage();
                                Parent root = null;
                                try {
                                    root = FXMLLoader.load(getClass().getResource("/fxml/sys/edit_dep.fxml"));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                stage.setScene(new Scene(root, 600, 400));
                                stage.getIcons().add(new Image(getClass().getResource("/static/logo.png").toString()));
                                stage.setResizable(false);
                                stage.setTitle("修改部门");
                                stage.show();
                            }
                        });
                        Insets insets = new Insets(0, 0, 0, 15);
                        hBox.setPadding(insets);
                        hBox.setSpacing(20);
                        hBox.getChildren().setAll(del, edit);
                        return hBox;
                    }
                };
                return observableValue;
            }
        });
        depTab.getItems().addAll(Constant.depList);
    }

    /**
     * 新增部门
     *
     * @param actionEvent
     */
    public void addDepAction(ActionEvent actionEvent) throws IOException {
        depRelod(null);
        Stage stage = Constant.getStage();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/sys/add_dep.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.getIcons().add(new Image(getClass().getResource("/static/logo.png").toString()));
        stage.setResizable(false);
        stage.setTitle("新增部门");
        stage.show();
    }

    public void depRelod(ActionEvent actionEvent) {
        Result<List<OaDep>> depList = HttpClient.doGet("/dep/list/v1", null, new TypeReference<Result<List<OaDep>>>() {
        });
        Constant.depList = depList.getData();
        depTab.getItems().setAll(Constant.depList);
        que_dep.getItems().setAll(Constant.depList);
    }

    public void queAction(ActionEvent actionEvent) throws JsonProcessingException {
        queryUser.setName(qu_name.getText());
        queryUser.setDep(Objects.isNull(que_dep.getValue()) ? "" : que_dep.getValue().getName());
        Result<List<OaUser>> result = HttpClient.doPost("/user/list/v1", queryUser, new TypeReference<Result<List<OaUser>>>() {
        });
        list = result.getData();
        userTbl.getItems().setAll(list);
    }

    public void cleanAction(ActionEvent actionEvent) {
        qu_name.setText("");
        que_dep.setValue(null);
    }
}
