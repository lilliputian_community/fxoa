package com.fx.oa.login;


public class LoginUser {

    private String name;

    private String password;

    private String ip;

    public LoginUser() {
    }

    public LoginUser(String name, String password, String ip) {
        this.name = name;
        this.password = password;
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "{" +
                "'name':'" + name + '\'' +
                ", 'password':'" + password + '\'' +
                ", 'ip':'" + ip + '\'' +
                '}';
    }
}
