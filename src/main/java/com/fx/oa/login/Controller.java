package com.fx.oa.login;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fx.oa.http.HttpClient;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public TextField account;

    public PasswordField password;
    public Label ipCode;

    /**
     * 登录
     *
     * @param actionEvent
     * @throws IOException
     */
    public void login(ActionEvent actionEvent) throws IOException {
//        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/fxml/home.fxml")));
//        account.getScene().setRoot(root);

        if (!StringUtils.hasLength(account.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "请输入账号", "请输入账号");
        }
        if (!StringUtils.hasLength(password.getText())) {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "请输入密码", "请输入密码");
        }
        LoginUser loginUser = new LoginUser(account.getText(), password.getText(), Constant.IP);
        Result<OaUser> result = HttpClient.doPost("/login/v1", loginUser, new TypeReference<Result<OaUser>>() {
        });
        if (result.getSuccess()) {
            Parent root;
            Constant.oaUser = result.getData();
            if (Constant.oaUser.getAdmin()) {
                root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/fxml/home.fxml")));
            } else {
                root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/fxml/clocking.fxml")));
            }
            account.getScene().setRoot(root);
        } else {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, result.getMsg(), result.getMsg());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ipCode.setText(Constant.IP);
    }
}
