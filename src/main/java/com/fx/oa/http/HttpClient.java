package com.fx.oa.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fx.oa.login.OaUser;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

public class HttpClient {

    private static RestTemplate restTemplate;

    private static ObjectMapper objectMapper;

    public HttpClient() {
    }

    public static RestTemplate getRestTemplate() {
        if (restTemplate == null) {
            synchronized (RestTemplate.class) {
                if (restTemplate == null) {
                    restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
                }
            }
        }
        return restTemplate;
    }

    public static ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            synchronized (ObjectMapper.class) {
                if (objectMapper == null) {
                    objectMapper = new ObjectMapper();
                }
            }
        }
        return objectMapper;
    }


    /**
     * get请求
     *
     * @param url          url，不含域名
     * @param urlVariables 请求参数
     * @param <T>
     * @return
     */
    public static <T> T doGet(String url, Map<String, ?> urlVariables, TypeReference<T> valueTypeRef) {
        ResponseEntity<String> responseEntity;
        if (Objects.isNull(urlVariables)) {
            responseEntity = HttpClient.getRestTemplate().getForEntity(Constant.HTTP_IP.concat(url), String.class);
        } else {
            responseEntity = HttpClient.getRestTemplate().getForEntity(Constant.HTTP_IP.concat(url), String.class, urlVariables);
        }
        return typeCast(responseEntity, valueTypeRef);
    }

    /**
     * post请求
     *
     * @param url  url，不含域名
     * @param body body中应该传入的参数
     * @param <T>
     * @return
     */
    public static <T> T doPost(String url, Object body, TypeReference<T> valueTypeRef) throws JsonProcessingException {
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> request = new HttpEntity<>(HttpClient.getObjectMapper().writeValueAsString(body), headers);
        ResponseEntity<String> responseEntity = HttpClient.getRestTemplate().postForEntity(Constant.HTTP_IP.concat(url), request, String.class);
        return typeCast(responseEntity, valueTypeRef);
    }

    /**
     * 类型转换
     *
     * @param responseEntity
     * @param <T>
     * @return
     */
    private static <T> T typeCast(ResponseEntity<String> responseEntity, TypeReference<T> valueTypeRef) {
        T result = null;
        try {
            result = HttpClient.getObjectMapper().readValue(responseEntity.getBody(), valueTypeRef);
        } catch (JsonProcessingException e) {
            System.out.println("类型转换失败");
            e.printStackTrace();
        }
        return result;
    }


    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(Constant.HTTP_IP + "/test/v1", String.class, (Object) null);
            System.out.println(responseEntity.getBody());
            ObjectMapper objectMapper = new ObjectMapper();
            Result<OaUser> result = objectMapper.readValue(responseEntity.getBody(), new TypeReference<Result<OaUser>>() {
            });
            System.out.println(result.getData());
        } catch (RestClientException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }


}
