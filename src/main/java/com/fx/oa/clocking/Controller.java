package com.fx.oa.clocking;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fx.oa.clocking.model.ClockRecord;
import com.fx.oa.http.HttpClient;
import com.fx.oa.util.Constant;
import com.fx.oa.util.Result;
import com.fx.oa.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public VBox vboxId;
    public Label dateTime;


    List<ClockRecord> list = new ArrayList<>(7);
    /**
     * 部门
     */
    public Label dep;
    public Label name;
    public Label weekTime;

    public TableView<ClockRecord> record;
    public TableColumn<ClockRecord, String> tDate;
    public TableColumn<ClockRecord, String> dutyTime;
    public TableColumn<ClockRecord, String> offDutyTime;
    public TableColumn<ClockRecord, String> clockingTime;

    /**
     * 打卡
     *
     * @param actionEvent
     */
    public void clockIn(ActionEvent actionEvent) throws JsonProcessingException {
        Result result = HttpClient.doPost("/user/clock/v1", Constant.oaUser, new TypeReference<Result>() {
        });
        if (result.getSuccess()) {
            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "上班打卡", "上班打卡成功");
            Result<List<ClockRecord>> resultList = HttpClient.doGet("/user/clock/" + Constant.oaUser.getId() + "/v1", null, new TypeReference<Result<List<ClockRecord>>>() {
            });
            list = resultList.getData();
            record.getItems().setAll(list);
        } else {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "打卡失败，请重试", result.getMsg());
        }
    }

    /**
     * 请假
     *
     * @param actionEvent
     */
    public void leave(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/fxml/leave.fxml")));
        dep.getScene().setRoot(root);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dep.setText(Constant.oaUser.getDep());
        name.setText(Constant.oaUser.getName());
        LocalDate localDate = LocalDate.now();
        dateTime.setText(localDate.getYear() + "年" + dayFormat(localDate.getMonthValue()) + "月" + dayFormat(localDate.getDayOfMonth()) + "日");
        weekTime.setText("星期" + weekFormat(localDate.getDayOfWeek().getValue()));
        tDate.setCellValueFactory(new PropertyValueFactory<>("clock"));
        dutyTime.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        offDutyTime.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        clockingTime.setCellValueFactory(new PropertyValueFactory<>("clockTime"));
        Result<List<ClockRecord>> result = HttpClient.doGet("/user/clock/" + Constant.oaUser.getId() + "/v1", null, new TypeReference<Result<List<ClockRecord>>>() {
        });
        list = result.getData();
        record.getItems().setAll(list);
    }

    private String dayFormat(int day) {
        if (day < 10) {
            return "0" + day;
        } else {
            return Integer.toString(day);
        }
    }

    private String weekFormat(int week) {
        switch (week) {
            case 1:
                return "一";
            case 2:
                return "二";
            case 3:
                return "三";
            case 4:
                return "四";
            case 5:
                return "五";
            case 6:
                return "六";
            default:
                return "天";
        }
    }

    /**
     * 退出到首页
     *
     * @param actionEvent
     */
    public void outAction(ActionEvent actionEvent) throws IOException {
        boolean out = Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "退出", "确认退出？");
        if (out) {
            Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/fxml/login.fxml")));
            dep.getScene().setRoot(root);
        }
    }

    public void clockOff(ActionEvent actionEvent) throws JsonProcessingException {
        Result result = HttpClient.doPost("/user/clock/off/v1", Constant.oaUser, new TypeReference<Result>() {
        });
        if (result.getSuccess()) {
            Util.f_alert_confirmDialog(Alert.AlertType.INFORMATION, "下班打卡", "下班打卡成功");
            Result<List<ClockRecord>> resultList = HttpClient.doGet("/user/clock/" + Constant.oaUser.getId() + "/v1", null, new TypeReference<Result<List<ClockRecord>>>() {
            });
            list = resultList.getData();
            record.getItems().setAll(list);
        } else {
            Util.f_alert_confirmDialog(Alert.AlertType.ERROR, "打卡失败，请重试", result.getMsg());
        }
    }
}
